package io.jplus.admin.service.impl;

import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.ScheduleJob;
import io.jplus.admin.service.ScheduleJobService;


@Bean
@RPCBean
public class ScheduleJobServiceImpl extends JbootServiceBase<ScheduleJob> implements ScheduleJobService {

}