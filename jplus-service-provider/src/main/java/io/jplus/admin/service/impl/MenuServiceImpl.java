package io.jplus.admin.service.impl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.cache.annotation.CacheEvict;
import io.jboot.components.cache.annotation.CachePut;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Column;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Menu;
import io.jplus.admin.service.MenuService;
import io.jplus.admin.service.RoleMenuService;

import java.util.ArrayList;
import java.util.List;


@Bean
@RPCBean
public class MenuServiceImpl extends JbootServiceBase<Menu> implements MenuService {

    @Inject
    RoleMenuService roleMenuService;

    @Override
    public List<Menu> findByColumns(Columns columns) {
        List<Menu> menuList = DAO.findListByColumns(columns);
        for (Menu menu : menuList) {
            Menu parent = findById(menu.getPid());
            if (parent != null) {
                menu.setPname(parent.getName());
            }
        }
        return menuList;
    }

    @Override
    @CachePut(name = "menuList",key = "#(userId)")
    public List<Menu> findListByUserId(String userId) {
        //系统管理员，拥有最高权限
        if (JplusConsts.SUPER_ADMIN.equals(userId)) {
            return getAllMenuList(null);
        }
        List<String> menuIdList = roleMenuService.queryAllMenuId(userId);
        return getAllMenuList(menuIdList);
    }

    @Override
    public List<Menu> findListParentId(String parentId, List<String> menuIdList) {
        List<Menu> menuList = findListParentId(parentId);
        if (menuIdList == null) {
            return menuList;
        }

        List<Menu> userMenuList = new ArrayList<>();
        for (Menu menu : menuList) {
            if (menuIdList.contains(menu.getId())) {
                userMenuList.add(menu);
            }
        }
        return userMenuList;
    }

    @Override
    public List<Menu> findNotButtonList() {
        SqlPara sqlPara = Db.getSqlPara("admin-menu.queryNotButtonList");
        return DAO.find(sqlPara);
    }

    @Override
    public Menu findMenuById(Object id) {
        Menu menu = findById(id);
        Menu parent = findById(menu.getPid());
        if (parent != null) {
            menu.setPname(parent.getName());
        }
        return menu;
    }

    @Override
    public List<Menu> findListParentId(String pid) {
        return DAO.findListByColumn(Column.create("pid", pid));
    }

    private List<Menu> getAllMenuList(List<String> menuIdList) {
        //查询根菜单列表
        List<Menu> menuList = findListParentId("0", menuIdList);
        //递归获取子菜单
        getMenuTreeList(menuList, menuIdList);
        return menuList;
    }

    private List<Menu> getMenuTreeList(List<Menu> menuList, List<String> menuIdList) {
        List<Menu> subMenuList = new ArrayList<Menu>();

        for (Menu entity : menuList) {
            //目录
            if (entity.getType() == JplusConsts.MenuType.CATALOG.getValue()) {
                entity.setList(getMenuTreeList(findListParentId(entity.getId(), menuIdList), menuIdList));
                //entity.put("list",getMenuTreeList(findListParentId(entity.getMenuId(), menuIdList), menuIdList));
            }
            subMenuList.add(entity);
        }

        return subMenuList;
    }

    @Override
    @CacheEvict(name = "menuList")
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}