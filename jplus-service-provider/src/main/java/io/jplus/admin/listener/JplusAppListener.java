package io.jplus.admin.listener;

import com.jfinal.plugin.druid.DruidStatViewHandler;
import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.core.listener.JbootAppListenerBase;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Title: Jplus listener
 * @Package io.jplus.admin.listener
 * @create 2019-01-03 14:57
 */
public class JplusAppListener extends JbootAppListenerBase {

    @Override
    public void onHandlerConfig(JfinalHandlers handlers) {
        super.onHandlerConfig(handlers);
        handlers.add(0, new DruidStatViewHandler("/admin/druid"));
    }
}
