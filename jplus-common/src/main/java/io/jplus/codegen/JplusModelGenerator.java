/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.codegen;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.model.JbootBaseModelGenerator;
import io.jboot.codegen.model.JbootModelGenerator;
import io.jboot.utils.StrUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by hello on 2018/1/8.
 */
public class JplusModelGenerator extends JbootModelGenerator {

    public JplusModelGenerator(String modelPackageName, String baseModelPackageName, String modelOutputDir) {
        super(modelPackageName, baseModelPackageName, modelOutputDir);
    }


    public static void run(String basePackage, String prefix) {
        doGenerate(basePackage, prefix);
    }

    /**
     * 只生成某些表
     */
    public static void runWith(String modelPackage, String includeTables) {
        doGenerateWith(modelPackage, includeTables);
    }

    /**
     * 只生成某些表,排除无主键表
     */
    public static void runWith(String modelPackage, String includeTables, String excludeTables) {
        doGenerateWith(modelPackage, includeTables, excludeTables, null);
    }

    /**
     * 只生成某些表,排除无主键表
     */
    public static void runWith(String modelPackage, String includeTables, String excludeTables, String prefix) {
        doGenerateWith(modelPackage, includeTables, excludeTables, prefix);
    }

    public static void doGenerate(String basePackage, String prefix) {
        doGenerateWith(basePackage, null, null, prefix);
    }


    /**
     * 只生某些表
     */
    public static void doGenerateWith(String basePackage, String includeTables) {
        doGenerateWith(basePackage, includeTables, null, null);
    }

    /**
     * 只生某些表
     */
    public static void doGenerateWith(String basePackage, String includeTables, String excludeTables, String prefix) {

        String modelPackage = basePackage;
        String baseModelPackage = basePackage + ".base";

        String modelDir = PathKit.getWebRootPath() + "/src/main/java/" + modelPackage.replace(".", "/");
        String baseModelDir = PathKit.getWebRootPath() + "/src/main/java/" + baseModelPackage.replace(".", "/");

        System.out.println("start generate...");
        System.out.println("generate dir:" + modelDir);
        MetaBuilder metaBuilder = CodeGenHelpler.createMetaBuilder();
        metaBuilder.setRemovedTableNamePrefixes(StrUtil.splitToSet(prefix, ",").toArray(new String[]{}));
        metaBuilder.addExcludedTable(StrUtil.splitToSet(excludeTables, ",").toArray(new String[]{}));
        List<TableMeta> tableMetaList = metaBuilder.build();
        withIncludeTables(tableMetaList, includeTables);

        new JbootBaseModelGenerator(baseModelPackage, baseModelDir).generate(tableMetaList);
        new JbootModelGenerator(modelPackage, baseModelPackage, modelDir).generate(tableMetaList);

        System.out.println("model generate finished !!!");

    }

    /**
     * 指定表
     *
     * @param list
     * @param includeTables
     */
    public static void withIncludeTables(List<TableMeta> list, String includeTables) {
        if (StringUtils.isNotBlank(includeTables)) {
            List<TableMeta> newTableMetaList = new ArrayList<>();
            Set<String> includeTableSet = StrUtil.splitToSet(includeTables.toLowerCase(), ",");

            for (TableMeta tableMeta : list) {
                if (includeTableSet.contains(tableMeta.name.toLowerCase())) {
                    newTableMetaList.add(tableMeta);
                }
            }
            list.clear();
            list.addAll(newTableMetaList);
        }
    }

}
