package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.RoleMenu;

import java.util.List;

public interface RoleMenuService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public RoleMenu findById(Object id);


    /**
     * find all model
     *
     * @return all <RoleMenu
     */
    public List<RoleMenu> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(RoleMenu model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(RoleMenu model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(RoleMenu model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(RoleMenu model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<RoleMenu> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<RoleMenu> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<RoleMenu> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    public List<String> queryAllMenuId(String userId);

    public List<RoleMenu> findListByRoleId(Object roleId);

    public boolean deleteByRoleId(Object id);

    public boolean deleteById(Object... ids);
}