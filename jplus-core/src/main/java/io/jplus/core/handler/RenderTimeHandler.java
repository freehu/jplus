/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.handler;

import com.jfinal.handler.Handler;
import com.jfinal.log.Log;
import io.jboot.Jboot;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RenderTimeHandler extends Handler {

    protected final Log log = Log.getLog(RenderTimeHandler.class);

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (Jboot.isDevMode()) {
            long start = System.currentTimeMillis();
            next.handle(target, request, response, isHandled);
            long end = System.currentTimeMillis();
            log.info("rending time:" + (end - start) + "ms;  target:" + target);
        } else {
            next.handle(target, request, response, isHandled);
        }
    }

}